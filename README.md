# javascriptWeb3D

#### 介绍
模型观察者-model-viewer Demo

#### 使用说明

1. 部分模型较大，加载可能有点慢，请耐心等待!
2. js部分使用class类写法，如不适应，可以修改成普通方法!



English:
#### introduce

Model viewer demo



#### instructions

1. Some models are large, and loading may be a little slow. Please wait patiently!

2. JS part uses the class class writing method. If it is not suitable, it can be modified into a common method!